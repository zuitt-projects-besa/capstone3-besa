const express = require('express');
const mongoose = require('mongoose')
const cors = require('cors');
const port = process.env.PORT || 4000;
const app = express();

// Routes
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")


// Connect DB
mongoose.connect("mongodb+srv://admin_besa:admin169@besa-169.lzfjc.mongodb.net/onlineMusicStore?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true	
});

// Check
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'))

// Middleware
app.use(express.json());
app.use(cors());
app.use('/users', userRoutes)
app.use('/products', productRoutes)

app.listen(port, () => console.log(`Server is running at port ${port}`));