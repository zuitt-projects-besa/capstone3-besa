const mongoose = require ('mongoose');

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	order: [
		{
			orderId: {
				type: String,
				required: [true, "orderId is required"]
			},
			totalAmount: {
				type: Number,
				default: 1
			},
			name: {
				type: String,
				default: "Drums"
			},
			price: {
				type: Number,
				default: 1000
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}

		}
	]
	
});

module.exports = mongoose.model("User", userSchema)
