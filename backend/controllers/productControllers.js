const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");

// Create Product
module.exports.createProduct = (req, res) => {

	Product.findOne({name: req.body.name}).then(result => {
		if (result != null && result.name == req.body.name){

			return false
			// return res.send('Duplicate product name')

		} else {

			let newProduct = new Product ({
				name : req.body.name,
				description : req.body.description,
				price : req.body.price,
			});

			newProduct.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
};

// Retrieve Active Products
module.exports.retrieveActiveProducts = (req, res) => {
	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


// Retrieve All Active Products
module.exports.retrieveProducts = (req, res) => {
	return Product.find({isActive: true})
	.then(result => {
		return result
	})
};

module.exports.retrieveAllProducts = (req, res) => {
	return Product.find({})
	.then(result => {
		return result
	})
};

// Retrieve Single Product
module.exports.retrieveSingleProduct = (reqParams) => {
	
	return Product.findById(reqParams.productId)
	.then(result => {
		return result
	})
};

module.exports.deleteProduct = (reqParams) => {
	return Product.findByIdAndDelete(reqParams.productId)
	.then(result => {
		return result
	})
};

module.exports.getSingleProduct = (reqParams) => {
	
	return Product.findById(reqParams.productId)
	.then(result => {
		return result
	})

};

// Update Product
module.exports.updateProduct = (req, res) => {

	let updates = {
		name : req.body.name,
		description : req.body.description,
		price : req.body.price,
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

// Archive Product
module.exports.archiveProduct = (req, res) => {

	let updates = {
		isActive : false
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Activate Product
module.exports.activateProduct = (req, res) => {

	let updates = {
		isActive : true
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Retrieve Product Buyers
module.exports.retrieveProductBuyer = (req, res) => {
	console.log(req.params.id)

	Product.findById(req.params.id)
	.then(result => {
		res.send(result.buyer)
	})
	.catch(error => res.send(error))
}
