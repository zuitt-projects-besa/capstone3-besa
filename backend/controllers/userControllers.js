const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const User = require("../models/User");

// Register
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	
    //salt => number of iterations that the data will undergo to mask the value.
	//MASK the real of the data. 

	return newUser.save().then((user, err) => {

		if(err){

			 return false

		} else {

			return true
		}
	})
}

module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){

			return true

		} else {
			 return false
		}
	})
}


// Login

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email})
	.then(result => {
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}

		}
	})
};


module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		console.log("result.password")
		console.log(result.password)

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		
		result.password = "";
		console.log("result.password2")

		console.log(result.password)
		// Returns the user information with the password as an empty string
		return result;

	});

};

// Update User To Admin
module.exports.updateToAdmin = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));
};


// Show Users
module.exports.showUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.buy = async (data) => {


	if(data.isAdmin === true){

		 return false

	} else {

		let isUserUpdated = await User.findById(data.userId).then(user => {

			user.order.push({
				orderId : data.orderId,
				totalAmount : data.totalAmount,
				name : data.name,
				price : data.price
			})
			
			return user.save().then((user, err) => {

				if(err){

					return false

				} else {

					return true
				}
			})
		})

		let isProductUpdated = await Product.findById(data.orderId).then(product => {

			product.buyer.push({
				buyerId : data.userId,
				totalAmount : data.totalAmount
			})

			return product.save().then((product, err) => {

				if(err){

					 return false

				} else {

					return true
				}
			})
		})

		if(isUserUpdated && isProductUpdated){

			return true

		} else {

			return false
		}
	}
}


module.exports.retrieveUserOrders = (req, res) => {
	User.findById(req.user.id)
	.then(result =>{
		return res.send(result.order)
	})
	.catch(error => res.send(error))
};

module.exports.remove = (req, res) => {
	User.findById(req.user.id)
	.then (user => {
		user.order.splice(user.order.findIndex(i => {
			return i.orderId === req.params.id
		}), 1);

		return user.save()
		.then(order=>res.send(order))
		.catch(err => res.send(err))
	})
};