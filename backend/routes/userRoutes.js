const express = require('express');
const router = express.Router();
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

// import user controllers
const userControllers = require("../controllers/userControllers");

// Routes

router.post('/register', (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/checkEmail", (req, res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/login', (req, res) => {
	userControllers.loginUser(req.body)
	.then(resultFromController => res.send(resultFromController))
})

router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
		
	userControllers.getProfile({userId : userData.id})
	.then(resultFromController => res.send(resultFromController));

});

router.post("/buy", auth.verify, (req, res) => {

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		orderId : req.body.productId,
		totalAmount : req.body.totalAmount,
		name : req.body.name,
		price : req.body.price
	}

	userControllers.buy(data).then(resultFromController => res.send(resultFromController))
})

router.put("/updateToAdmin/:id", verify, verifyAdmin, userControllers.updateToAdmin)

router.get("/showUsers", userControllers.showUsers)

router.get("/retrieveUserOrders", verify, userControllers.retrieveUserOrders)

router.put("/remove/:id", verify, userControllers.remove)


module.exports = router;