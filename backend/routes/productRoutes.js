const express = require('express');
const router = express.Router();
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

// import product controllers
const productControllers = require("../controllers/productControllers");

// Routes
router.post("/createProduct", verify, verifyAdmin, productControllers.createProduct);

router.get("/retrieveActiveProducts", productControllers.retrieveActiveProducts);

router.get('/', (req, res) => {
	productControllers.retrieveProducts()
	.then(resultFromController => res.send(resultFromController))
});

router.get('/all', (req, res) => {
	productControllers.retrieveAllProducts()
	.then(resultFromController => res.send(resultFromController))
});

router.get('/:productId', (req, res) => {
	productControllers.retrieveSingleProduct(req.params)
	.then(resultFromController => res.send(resultFromController))
});

router.delete('/delete/:productId',verify, verifyAdmin, (req, res) => {
	productControllers.deleteProduct(req.params)
	.then(resultFromController => res.send(resultFromController))
});

router.get('/get/:productId', (req, res) => {
	productControllers.getSingleProduct(req.params)
	.then(resultFromController => res.send(resultFromController))
});

router.put("/updateProduct/:id", verify, verifyAdmin, productControllers.updateProduct);

router.put("/archiveProduct/:id", verify, verifyAdmin, productControllers.archiveProduct);

router.put("/activateProduct/:id", verify, verifyAdmin, productControllers.activateProduct);

router.get("/retrieveProductBuyer/:id", verify, verifyAdmin, productControllers.retrieveProductBuyer);


module.exports = router;