import {useState, useEffect, useContext} from 'react'
import {Navigate, Link} from 'react-router-dom'
import {Form, Button, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'

import '../App.css'

import UserContext from '../UserContext'


export default function Login(props) {

	const {user, setUser} = useContext(UserContext)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {

		if (email !== '' && password !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[email, password, setIsActive])


function authenticate(e) {

	e.preventDefault();

	fetch('https://evening-island-65608.herokuapp.com/users/login', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			email: email,
			password: password
		})
	})
	.then(res => res.json())
	.then(data => {

		if (typeof data.access !== "undefined") {

			localStorage.setItem('token', data.access)
			retrieveUserDetails(data.access)

			Swal.fire({
				title: 'Login Successful',
				icon: 'success',
				text: 'Welcome to Music Avenue!',
				confirmButtonColor: 'green'
			})
		} else {
			Swal.fire({
				title: 'Authentication Failed',
				icon: 'error',
				text: 'Please check your credentials',
				confirmButtonColor: 'red'
			})
		}

	})
	
	setEmail('');
	setPassword('');

	const retrieveUserDetails = (token) => {
		fetch('https://evening-island-65608.herokuapp.com/users/details',{
			method: "POST",
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})
	}

}

	return(

		(user.id !== null) ?

		<Navigate to ="/"/>
		
		:

		<Container className="containerDesign mt-5">
		<Form className="m-3" onSubmit={e => authenticate(e)}>
		<h1 className="title py-3">Login</h1>
			<Form.Group controlId="email">
				<Form.Label>Email</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "Enter Email"
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Enter Password"
					value = {password}
					onChange = {e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
			
			{	isActive ?
				<Button type="submit" variant="dark" className="mt-3">
					Login
				</Button>
				:
				<Button type="submit" variant="dark" className="mt-3" disabled>
					Login
				</Button>
			}
			<Form.Group>
			<Form.Text as={Link} to="/register">No account yet?</Form.Text>
			</Form.Group>
				
		</Form>
		</Container>
		
	);
}