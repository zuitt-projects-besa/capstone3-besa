import {Container, Row, Col, Button, Card} from 'react-bootstrap'
import {Link, Navigate} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import CartCard from '../components/CartCard'

import UserContext from '../UserContext'


export default function Cart() {

	const {user} = useContext(UserContext)
	
	const [cart, setCart] = useState([])

	useEffect(() => {

		fetch(`https://evening-island-65608.herokuapp.com/users/retrieveUserOrders/`,{
			method: 'GET',
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setCart(data.map(product => {

				return (

					<CartCard key={product._id} cartProp={product}/>

				)
			}))

		})

	}, [])

	return(
		
		(user.isAdmin === true) ?

		<Navigate to ="/"/>
		
		:
	<>
		<h1 className="py-3 title">My Cart</h1>
		{cart}
	</>
	)
}