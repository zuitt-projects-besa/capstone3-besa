import {Navbar, Nav, Container,} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {useEffect} from 'react'
import React, { useState, useContext } from 'react'
import UserContext from '../UserContext'

export default function AppNavbar() {

const {user} = useContext(UserContext)

const {unsetUser, setUser} = useContext(UserContext);

	return (

	<Navbar bg="dark" expand="lg" variant="dark" className="p-4">
	    <Container>
		    <Navbar.Brand as = {Link} to="/">Music Avenue</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
			    <Nav className="ml-auto">

			      <Nav.Link as = {Link} to="/">Home</Nav.Link>
			      <Nav.Link as = {Link} to="/products">Product page</Nav.Link>
			      
			    {
			      (user.isAdmin === true)?
			      	<>
			      		<Nav.Link as = {Link} to="/dashboard">Dashboard</Nav.Link>
			      		<Nav.Link as = {Link} to="/logout">Logout</Nav.Link>
			      	</>
			      	:
			      	(user.isAdmin === false)?
			      	<>
			      		<Nav.Link as = {Link} to="/cart">Cart</Nav.Link>
			      		<Nav.Link as = {Link} to="/logout">Logout</Nav.Link>
			      	</>
				      :
				   	<>
				      <Nav.Link as = {Link} to="/login">Login</Nav.Link>
				      <Nav.Link as = {Link} to="/register">Register</Nav.Link>
				    </>
			    }

		    </Nav>
		    </Navbar.Collapse>
	    </Container>
  </Navbar>
	);
}