import {Container, Card, Button, Modal, Form, Nav} from 'react-bootstrap'
import {useState, useEffect, location} from 'react'
import {Link, useParams , useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import React from 'react'

export default function DashboardView({productProp}) {

	const {name, description, price, _id, isActive} = productProp;
	const history = useNavigate();
	const [status, setStatus] = useState('')

	const activateProduct = (productId, isActive) => {
		
		fetch(`https://evening-island-65608.herokuapp.com/products/activateProduct/${_id}`,{
		method: 'PUT',
		headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
		body: JSON.stringify({
			isActive: isActive
		})
		
	})
		.then(res => res.json())
		.then(data => {
			console.log("data")
			console.log(data)
		})

		Swal.fire({
				title: `${name}`,
				icon: 'Success',
				text: `${name} has been activated.`,
				confirmButtonColor: 'green'
			})
		history('/products')

	}

	const archiveProduct = (productId, isActive) => {

		Swal.fire({
		  title: `Archive ${name}?`,
		  showDenyButton: false,
		  showCancelButton: true,
		  text: "This item will not be shown in the product page",
		  confirmButtonColor: 'red',
		  confirmButtonText: 'Archive',
		  denyButtonText: `Don't save`,
		}).then((result) => {

		  if (result.isConfirmed) {
		    
		    fetch(`https://evening-island-65608.herokuapp.com/products/archiveProduct/${_id}`,{
				method: 'PUT',
				headers: {
						'Content-Type':'application/json',
						Authorization: `Bearer ${localStorage.getItem("token")}`
					},
				body: JSON.stringify({
					isActive: isActive
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log("data")
					console.log(data)
				})
				Swal.fire({
						title: `${name}`,
						icon: 'Success',
						text: `${name} has been archived.`,
						confirmButtonColor: 'green'
					})
				history('/products')

		  } else if (result.isDenied) {
		    Swal.fire('Changes are not saved', '', 'info')
		}
	})
}

const deleteProduct = () => {

	Swal.fire({
		  title: `Delete ${name}?`,
		  showDenyButton: false,
		  showCancelButton: true,
		  confirmButtonColor: 'red',
		  confirmButtonText: 'Delete',
		  denyButtonText: `Don't save`,
		}).then((result) => {

		  if (result.isConfirmed) {
		    
		  	fetch(`https://evening-island-65608.herokuapp.com/products/delete/${_id}`,{
				method: 'DELETE',
				headers: {
						'Content-Type':'application/json',
						Authorization: `Bearer ${localStorage.getItem("token")}`
					}
			})
			.then(res => res.json())
			.then(data => {
				console.log("DELETE DATA")
				console.log(typeof data)
				console.log(data)
			})

			Swal.fire({
					title: `${name} deleted successfully`,
					icon: "success",
					text: "View product page",
					confirmButtonColor: 'green'
			})
			history('/products')

		  } else if (result.isDenied) {
		    Swal.fire('Changes are not saved', '', 'info')
		}
	})
}

	const [active, setActive] = useState ("Active")

	useEffect(() =>{
		if (isActive === true) {
			setActive("Active")
		} else{
			setActive("Archived")
		}

	},[setActive])

	return (

	<Container className="mb-4">

		<Card>
		  <Card.Header>
		   <Card.Title className="m-3">{name}</Card.Title> 
		  </Card.Header>
		  <Card.Body>
		    <Card.Title>Description:</Card.Title>
		    <Card.Text>{description}</Card.Text>
		    <Card.Title>Price:</Card.Title>
		    <Card.Text>{price}</Card.Text>
		    <Card.Title>Status:</Card.Title>
		    <Card.Text>{active}</Card.Text>
		    <Button variant="warning" className="mx-2 px-5" as={Link} to={`/products/get/${_id}`} >Edit</Button>
		    <Button variant="dark" className="mx-2 px-5" onClick={deleteProduct} >Delete</Button>

		    { (isActive === true) ?
				<Button className="px-4 mx-2 m-3" variant="danger" onClick={archiveProduct} >Archive Product</Button>
				:
				<Button className="px-4 mx-2 m-3" variant="success"onClick={activateProduct}>Activate Product</Button>
				}

		  </Card.Body>
		</Card>

	</Container>

	)
}
