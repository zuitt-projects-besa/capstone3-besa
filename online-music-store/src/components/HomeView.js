import {Row, Col, Card, CardGroup, Button} from 'react-bootstrap'
import '../App.css';
import {Link} from 'react-router-dom'
import {useContext} from 'react'
import UserContext from '../UserContext'

export default function HomeView() {

const {user} = useContext(UserContext);

	return(
	<>
  
		<Row className="homeView mt-2">
			<Col className = "p-5 m-5">
				<h1>Music Avenue</h1>
				<h5 >Shop Now!</h5>
        { user.id !== null?
        <Button variant="outline-dark px-5 mt-2" as={Link} to={"/products"}>Shop Now!</Button>
        :
				<Button variant="outline-dark px-5 mt-2" as={Link} to={"/login"}>Login!</Button>
        }
			</Col>
		</Row>

<CardGroup className="mt-2 cards">
  <Card className="p-2">
    <Card.Img variant="top" src="https://images.pexels.com/photos/4087992/pexels-photo-4087992.jpeg?cs=srgb&dl=pexels-cottonbro-4087992.jpg&fm=jpg" />
    <Card.Body>
      <Card.Title>Drum Set</Card.Title>
      <Card.Text>
        The most popular type of drum set is the five-piece, as it gives you a nice amount of tonal diversity but in a relatively compact package, making them less cumbersome to move about.
      </Card.Text>
      <Card.Text>
		It’s important to note the word ‘piece’ only refers to drums and does not include cymbals and any extra hardware. It may seem counter-intuitive, but you could be using say twelve cymbals or cowbells, but that still wouldn’t change how many ‘pieces’ make up your kit.
		</Card.Text>
    </Card.Body>
    <Card.Footer>
      <Link className="text-muted" as={Link} to="/products">Get your drum set now!</Link>
    </Card.Footer>
  </Card>
  <Card className="p-2">
    <Card.Img variant="top" src="https://images.pexels.com/photos/4472061/pexels-photo-4472061.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" />
    <Card.Body>
      <Card.Title>Electric Guitar</Card.Title>
      <Card.Text>An electric guitar is a guitar that produces sound by vibrating strings over a pickup that converts the vibrations into electrical signals. Those signals are fed into an amplifier, which projects the musical performance at a wide range of volumes. Most pickups function using electromagnetic induction, although non-magnetic pickups exist on a small number of electric guitars.
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <Link className="text-muted" as={Link} to="/products">Get your electric guitar now!</Link>
    </Card.Footer>
  </Card>
  <Card className="p-2">
    <Card.Img variant="top" src="https://images.pexels.com/photos/7901950/pexels-photo-7901950.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" />
    <Card.Body>
      <Card.Title>Bass Guitar</Card.Title>
      <Card.Text>
        A bass guitar is a plucked string instrument built in the style of an electric guitar but producing lower frequencies. It produces sound when its metal bass strings vibrate over one or more magnetic pickups (although non-magnetic pickups are occasionally used as well). The pickups then transmit a signal, via instrument cable, into an amplifier, which allows the bass to be heard at a wide range of volumes.
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <Link className="text-muted" as={Link} to="/products">Get your bass guitar now!</Link>
    </Card.Footer>
  </Card>
</CardGroup>

<CardGroup className="mt-2 cards">
  <Card className="p-2">
    <Card.Img variant="top" src="https://images.pexels.com/photos/34221/violin-musical-instrument-music-sound.jpg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" />
    <Card.Body>
      <Card.Title>Violin</Card.Title>
      <Card.Text>
Violin, byname fiddle, bowed stringed musical instrument that evolved during the Renaissance from earlier bowed instruments: the medieval fiddle; its 16th-century Italian offshoot, the lira da braccio; and the rebec. The violin is probably the best known and most widely distributed musical instrument in the world.

      </Card.Text>
      <Card.Text>
Like its predecessors but unlike its cousin the viol, the violin has a fretless fingerboard. Its strings are hitched to tuning pegs and to a tailpiece passing over a bridge held in place by the pressure of the strings. The bridge transmits the strings’ vibrations to the violin belly, or soundboard, which is made of pine and amplifies the sound. 
    </Card.Text>
    </Card.Body>
    <Card.Footer>
      <Link className="text-muted" as={Link} to="/products">Get your violin now!</Link>
    </Card.Footer>
  </Card>
  <Card className="p-2">
    <Card.Img variant="top" src="https://images.pexels.com/photos/3971983/pexels-photo-3971983.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" />
    <Card.Body>
      <Card.Title>Piano</Card.Title>
      <Card.Text>While the Piano is the most famous of the keyboard instruments, it’s not the first. The piano is a more modern reinvention of the harpsichord, which is still around and will be featured later on in this list.
      </Card.Text>
      <Card.Text>However, the piano is by far the most played keyboard instrument.  The modern piano has 88 keys and is played sitting down. Originally, it was created to solve the musical problem of dynamics—musicians couldn’t control how loud or quiet their music was. 
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <Link className="text-muted" as={Link} to="/products">Get your piano now!</Link>
    </Card.Footer>
  </Card>
  <Card className="p-2">
    <Card.Img variant="top" src="https://images.pexels.com/photos/4709822/pexels-photo-4709822.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" />
    <Card.Body>
      <Card.Title>Saxophone</Card.Title>
      <Card.Text>
        A bass guitar is a plucked string instrument built in the style of an electric guitar but producing lower frequencies. It produces sound when its metal bass strings vibrate over one or more magnetic pickups (although non-magnetic pickups are occasionally used as well). The pickups then transmit a signal, via instrument cable, into an amplifier, which allows the bass to be heard at a wide range of volumes.
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <Link className="text-muted" as={Link} to="/products">Get your saxophone now!</Link>
    </Card.Footer>
  </Card>
</CardGroup>

<Card className="bg-dark text-white m-3 ">
  <Card.Img src="https://media.gettyimages.com/photos/american-jazz-drummer-buddy-rich-in-copenhagen-denmark-october-1970-picture-id1034260988?s=2048x2048" alt="Card image" />
  <Card.ImgOverlay>
    <Card.Title>Buddy Rich</Card.Title>
    <Card.Text>
      I think the drummer should sit back there and play some drums, and never mind about the tunes. Just get up there and wail behind whoever is sitting up there playing the solo. And this is what is lacking, definitely lacking in music today.
    </Card.Text>
    <Card.Text>The world's greatest drummer</Card.Text>
  </Card.ImgOverlay>
</Card>
	</>
	)
}