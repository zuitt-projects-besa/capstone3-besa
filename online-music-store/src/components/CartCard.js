import {Container, Row, Col, Button, Card} from 'react-bootstrap'
import {Link, useParams, useNavigate} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import '../App.css'
import Swal from 'sweetalert2'


export default function Cart({cartProp}) {

	const history = useNavigate();
	const {orderId, totalAmount, name, price, _id} = cartProp;
	const totalPrice = price * totalAmount


	function checkout() {
		
		Swal.fire({
		  title: 'Checkout!',
		  text: `${name}`,
		  icon: 'info',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Proceed Checkout',
		  confirmButtonColor: 'green'
		}).then((result) => {

		fetch(`https://evening-island-65608.herokuapp.com/users/remove/${orderId}`,{

		method: 'PUT',
		headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
		})
		.then(res => res.json())
		.then(data => {})

		  if (result.isConfirmed) {
		    Swal.fire({
		      itle: 'Are you sure?',
			  text: "Checkout Successful",
			  confirmButtonColor: 'green',
			  icon: 'success',
		    }
		    )
		  history("/")
		  }
		})
	}

	function removeOrder () {

		Swal.fire({
		  title: 'Remove Item from Cart?',
		  text: `${name}`,
		  icon: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Remove',
		  confirmButtonColor: 'black'

		}).then((result) => {
		  if (result.isConfirmed) {

		  	fetch(`https://evening-island-65608.herokuapp.com/users/remove/${orderId}`,{

			method: 'PUT',
			headers: {
					'Content-Type':'application/json',
					Authorization: `Bearer ${localStorage.getItem("token")}`
				},
			})
			.then(res => res.json())
			.then(data => {})

			    Swal.fire({
			      itle: 'Are you sure?',
				  text: "Item Removed Successfully",
				  confirmButtonColor: 'green',
				  icon: 'success',
			    }
		    )
		  	history("/products")
		  }
		})
	}

	return(

	<>
		<div class="table-responsive">
			<table id="cartTable" className="mb-3">
			  <tr>
			    <th>Item Name</th>
			    <th>Price</th>
			    <th>Quantity</th>
			    <th>Subtotal</th>
			    <td className="tableText">
			    <Button className='px-2' variant="danger" onClick={removeOrder}>Remove Item</Button>
			    </td>
			  </tr>
			  <tr>
			    <td>{name}</td>
			    <td>{price}</td>
			    <td>{totalAmount}</td>
			    <td>{totalPrice}</td>
			    <td className="tableCenter">
			    <Button variant="success px-4" onClick={checkout}>Checkout</Button>
			    </td>
			  </tr>
			</table>
		</div>
	</>
	
	)
}